#ifndef I2RAPPLICATION_H
#define I2RAPPLICATION_H

#include <QCoreApplication>

class I2RApplication : public QCoreApplication
{

    Q_OBJECT

public:

    static const int ERR_OK;
    static const int ERR_FAILED_LOAD_INPUT;
    static const int ERR_BAD_FORMAT;
    static const int ERR_FAILED_SAVE_OUTPUT;

    I2RApplication( int &argc, char **argv );
    virtual ~I2RApplication();

private:

    class I2RApplicationPrivate;
    I2RApplicationPrivate *d;

private slots:

    void execCommands();

};

#endif // I2RAPPLICATION_H
