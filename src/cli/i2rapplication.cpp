#include "i2rapplication.h"

#include "regionexporter.h"
#include "regionextractor.h"
#include "regionprocessing.h"
#include "regionrenderer.h"

#include <QDebug>
#include <QStringList>
#include <QTimer>

class I2RApplication::I2RApplicationPrivate
{

public:

    static const QString INPUT;
    static const QString OUTPUT;
    static const QString FORMAT;
    static const QString BOUNDING_BOX_JOINING;
    static const QString BOUNDING_SPHERE_JOINING;
    static const QString GRAVITY_JOINING;

    static const QString FORMAT_XML;
    static const QString FORMAT_VISUALIZATION;

    QString input;
    QString output;
    QString format;
    bool    useBoundingBoxJoining;
    bool    useBoundingSphereJoining;
    bool    useGravityJoining;

    bool doWork;

    I2RApplicationPrivate( I2RApplication *app )
        : input( QString() ),
          output( QString() ),
          format( QString() ),
          useBoundingBoxJoining( false ),
          useBoundingSphereJoining( false ),
          useGravityJoining( false ),
          doWork( true ),
          application( app )
    {

    }

    virtual ~I2RApplicationPrivate()
    {

    }

    void printHelp()
    {
        qDebug() <<"Usage: " << qApp->applicationName() << " [options]";
        qDebug() << "";
        qDebug() << INPUT << "FILE" << "Load image from FILE";
        qDebug() << OUTPUT << "FILE" << "Save result to FILE";
        qDebug() << FORMAT << "FMT" << "Use FMT as output format. Can be one of the following:";
        qDebug() << " - " << FORMAT_XML << "Write analyzed data as XML file";
        qDebug() << " - " << FORMAT_VISUALIZATION << "Write a visualization of the calculated data.";
        qDebug() << BOUNDING_BOX_JOINING << "Join regions using a simple bounding box test.";
        qDebug() << BOUNDING_SPHERE_JOINING << "Join regions using a simple bounding sphere test.";
        qDebug() << GRAVITY_JOINING << "Join regions based on their distance and mass.";
    }

private:

    I2RApplication *application;

};

const QString I2RApplication::I2RApplicationPrivate::INPUT                      = "--input";
const QString I2RApplication::I2RApplicationPrivate::OUTPUT                     = "--output";
const QString I2RApplication::I2RApplicationPrivate::FORMAT                     = "--format";
const QString I2RApplication::I2RApplicationPrivate::BOUNDING_BOX_JOINING       = "--bounding-box-joining";
const QString I2RApplication::I2RApplicationPrivate::BOUNDING_SPHERE_JOINING    = "--bounding-sphere-joining";
const QString I2RApplication::I2RApplicationPrivate::GRAVITY_JOINING            = "--gravity-joining";

const QString I2RApplication::I2RApplicationPrivate::FORMAT_XML                 = "xml";
const QString I2RApplication::I2RApplicationPrivate::FORMAT_VISUALIZATION       = "visualization";


const int I2RApplication::ERR_OK                    = 0;
const int I2RApplication::ERR_FAILED_LOAD_INPUT     = 1;
const int I2RApplication::ERR_BAD_FORMAT            = 2;
const int I2RApplication::ERR_FAILED_SAVE_OUTPUT    = 3;

I2RApplication::I2RApplication( int &argc, char**argv )
    : QCoreApplication( argc, argv ),
      d( new I2RApplicationPrivate( this ) )
{
    QStringList arguments = this->arguments();
    if ( arguments.length() <= 1 )
    {
        d->printHelp();
        d->doWork = false;

    } else
    {
        arguments.removeFirst();
        while ( arguments.length() > 0 )
        {
            if ( arguments.first() == I2RApplicationPrivate::INPUT )
            {
                arguments.removeFirst();
                if ( arguments.length() > 0 )
                {
                    d->input = arguments.first();
                    arguments.removeFirst();
                    continue;
                }
            } else if ( arguments.first() == I2RApplicationPrivate::OUTPUT )
            {
                arguments.removeFirst();
                if ( arguments.length() > 0 )
                {
                    d->output = arguments.first();
                    arguments.removeFirst();
                    continue;
                }
            } else if ( arguments.first() == I2RApplicationPrivate::FORMAT )
            {
                arguments.removeFirst();
                if ( arguments.length() > 0 )
                {
                    d->format = arguments.first();
                    arguments.removeFirst();
                    continue;
                }
            } else if ( arguments.first() == I2RApplicationPrivate::BOUNDING_BOX_JOINING )
            {
                arguments.removeFirst();
                d->useBoundingBoxJoining = true;
                continue;
            } else if ( arguments.first() == I2RApplicationPrivate::BOUNDING_SPHERE_JOINING )
            {
                arguments.removeFirst();
                d->useBoundingSphereJoining = true;
                continue;
            } else if ( arguments.first() == I2RApplicationPrivate::GRAVITY_JOINING )
            {
                arguments.removeFirst();
                d->useGravityJoining = true;
                continue;
            }
            qFatal( "Unknown option: %s", arguments.first().toAscii().data() );
        }
    }
        

    QTimer::singleShot( 0, this, SLOT(execCommands()) );
}

I2RApplication::~I2RApplication()
{
    delete d;
}

void I2RApplication::execCommands()
{
    if ( !( d->doWork ) )
    {
        exit( 0 );
    }
    if ( !( d->input.isNull() || d->output.isNull() ) )
    {
        qDebug() << "Loading image" << d->input;
        QImage image( d->input );
        if ( image.isNull() )
        {
            qDebug() << "Failed to load. Probably image format not supported.";
            exit( ERR_FAILED_LOAD_INPUT );
        }
        RegionExtractor extractor( this );
        qDebug() << "Going to extract regions...";
        extractor.extract( image );

        QList< RegionOfInterest * > regions = extractor.regions();
        if ( d->useBoundingBoxJoining )
        {
            regions = RegionProcessing::boundingBoxJoining( regions, this );
        }
        if ( d->useBoundingSphereJoining )
        {
            regions = RegionProcessing::boundingSphereJoining( regions, this );
        }
        if ( d->useGravityJoining )
        {
            regions = RegionProcessing::gravityJoining( regions, this );
        }

        if ( d->format == I2RApplicationPrivate::FORMAT_XML )
        {
            qDebug() << "Exporting to XML...";
            if ( !( RegionExporter::exportToXml( regions, d->output ) ) )
            {
                exit( ERR_FAILED_SAVE_OUTPUT );
            }
        } else if ( d->format == I2RApplicationPrivate::FORMAT_VISUALIZATION )
        {
            qDebug() << "Exporting to visualization...";
            QImage visualization = RegionRenderer::render( image, regions );
            qDebug() << "Rendered. Going to save visualization...";
            if ( !( visualization.save( d->output ) ) )
            {
                exit( ERR_FAILED_SAVE_OUTPUT );
            }
        } else
        {
            qDebug() << "Invalid format" << d->format << "specified!";
            exit( ERR_BAD_FORMAT );
        }
    }
    exit( ERR_OK );
}
