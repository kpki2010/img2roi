TEMPLATE = app
TARGET = img2roi
HEADERS += i2rapplication.h
SOURCES += i2rapplication.cpp \
    main.cpp
INCLUDEPATH += ../lib
QT += core \
    gui
CONFIG(debug, debug|release):DESTDIR = ../../bin/Debug
else:DESTDIR = ../../bin/Release
CONFIG(debug, debug|release):LIBS += -L../../bin/Debug
else:LIBS += -L../../bin/Release
LIBS += -limg2roiutils
