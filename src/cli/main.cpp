#include "i2rapplication.h"

int main( int argc, char **argv )
{
    I2RApplication app( argc, argv );
    int result = app.exec();
    return result;
}
