/*
    img2roi - Region of Interest extractor
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REGIONRENDERER_H
#define REGIONRENDERER_H

#include "i2r_defines.h"

#include <QImage>
#include <QList>

class RegionOfInterest;

/**
  * @brief Render region information.
  *
  * The RegionRenderer class provides functionality to display calculated information
  * extracted from an image.
  */
class I2R_EXPORT RegionRenderer
{

public:

    /**
      * Ctor
      */
    virtual ~RegionRenderer();

    /**
      * @brief Render data to image.
      *
      * This method takes the original image and the regions extracted from it and
      * create a new image, which is made up of the original one (as background) and
      * visualizations of the calculated regions.
      */
    static QImage render( const QImage &original, const QList< RegionOfInterest * > &regions );

private:

    /*
      * Hide constructors. This is a abstract class, which only provides functions but
      * no internal data!
      */
    RegionRenderer();
    RegionRenderer( const RegionRenderer & );

};

#endif // REGIONRENDERER_H
