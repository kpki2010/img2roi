/*
    img2roi - Region of Interest extractor
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "regionofinterest.h"

#include <qmath.h>

#include <limits.h>

#define sqr(a) (a*a)

class RegionOfInterest::RegionOfInterestPrivate
{

public:

    QList< QPoint > pixels;
    QPoint          centerOfGravity;
    QRect           boundingBox;
    QPoint          boundingCircleCenter;
    int             boundingCircleRadius;
    QPoint          northPole;
    QPoint          southPole;

    RegionOfInterestPrivate( RegionOfInterest *region )
        : regionOfInterest( region )
    {

    }

    virtual ~RegionOfInterestPrivate()
    {

    }

    void recalculate()
    {
        recalculateBoundingBoxAndCenterOfGravity();
        recalculateBoundingSphere();
    }

private:

    RegionOfInterest *regionOfInterest;

    void recalculateBoundingBoxAndCenterOfGravity()
    {
        QRect bb;
        QPoint cog( 0, 0 );
        for ( int i = 0; i < pixels.length(); i++ )
        {
            if ( !( bb.isValid() ) )
            {
                bb.setRect( pixels[ i ].x(), pixels[ i ].y(), 0, 0 );
            }
            if ( bb.left() > pixels[ i ].x() )
            {
                bb.setLeft( pixels[ i ].x() );
            }
            if ( bb.right() < pixels[ i ].x() )
            {
                bb.setRight( pixels[ i ].x() );
            }
            if ( bb.top() > pixels[ i ].y() )
            {
                bb.setTop( pixels[ i ].y() );
            }
            if ( bb.bottom() < pixels[ i ].y() )
            {
                bb.setBottom( pixels[ i ].y() );
            }
            cog.setX( cog.x() + pixels[ i ].x() );
            cog.setY( cog.y() + pixels[ i ].y() );
        }
        boundingBox = bb;
        if ( pixels.length() > 0 )
        {
            cog.setX( cog.x() / pixels.length() );
            cog.setY( cog.y() / pixels.length() );
            centerOfGravity = cog;
        } else
        {
            cog = QPoint();
        }
    }

    void recalculateBoundingSphere()
    {
        QPoint p1 = pixels[ 0 ],
               p2 = pixels[ 0 ];
        double diameter = 0.0;
        bool finished = false;
        while ( !( finished ) )
        {
            finished = true;
            for ( int i = 0; i < pixels.length(); i++ )
            {
                QPoint p = pixels[ i ];
                double d1 = sqrt( sqr( ( double ) ( p1.x() - p.x() ) ) +
                                  sqr( ( double ) ( p1.y() - p.y() ) ) ),
                       d2 = sqrt( sqr( ( double ) ( p2.x() - p.x() ) ) +
                                  sqr( ( double ) ( p2.y() - p.y() ) ) );
                if ( d1 > d2 )
                {
                    if ( d1 > diameter )
                    {
                        p2 = p;
                        diameter = d1;
                        finished = false;
                    }
                } else
                {
                    if ( d2 > diameter )
                    {
                        p1 = p;
                        diameter = d2;
                        finished = false;
                    }
                }
            }
        }
        boundingCircleRadius = diameter / 2.0;
        boundingCircleCenter = p1 + ( p2 - p1 ) / 2;
        northPole = ( p1.y() < p2.y() ? p1 : p2 );
        southPole = ( p1.y() < p2.y() ? p2 : p1 );
    }

};

RegionOfInterest::RegionOfInterest( QObject *parent )
    : QObject( parent ),
      d( new RegionOfInterestPrivate( this ) )
{
}

RegionOfInterest::~RegionOfInterest()
{
    delete d;
}

const QList< QPoint > &RegionOfInterest::pixels() const
{
    return d->pixels;
}

const QPoint &RegionOfInterest::centerOfGravity() const
{
    return d->centerOfGravity;
}

const QRect &RegionOfInterest::boundingBox() const
{
    return d->boundingBox;
}

const QPoint &RegionOfInterest::boundingCircleCenter() const
{
    return d->boundingCircleCenter;
}

const int &RegionOfInterest::boundingCircleRadius() const
{
    return d->boundingCircleRadius;
}

const QPoint &RegionOfInterest::northPole() const
{
    return d->northPole;
}

const QPoint &RegionOfInterest::southPole() const
{
    return d->southPole;
}

void RegionOfInterest::setPixels( const QList< QPoint > &p )
{
    d->pixels = p;
    d->recalculate();
}

void RegionOfInterest::addPixel( const QPoint &p )
{
    if ( !( d->pixels.contains( p ) ) )
    {
        d->pixels.append( p );
        d->recalculate();
    }
}

void RegionOfInterest::addPixels( const QList<QPoint> &p )
{
    foreach ( QPoint pixel, p )
    {
        if ( !( d->pixels.contains( pixel ) ) )
        {
            d->pixels.append( pixel );
        }
    }
    d->recalculate();
}

void RegionOfInterest::removePixel( const QPoint &p )
{
    if ( d->pixels.contains( p ) )
    {
        d->pixels.removeAll( p );
        d->recalculate();
    }
}

void RegionOfInterest::clearPixels()
{
    d->pixels.clear();
    d->recalculate();
}
