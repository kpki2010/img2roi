/*
    img2roi - Region of Interest extractor
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "regionmapping.h"

#include <QDomDocument>
#include <QFile>

const QString RegionMapping::XMLE_MAPPING = "mapping";
const QString RegionMapping::XMLE_PAIR = "pair";
const QString RegionMapping::XMLE_REGION1 = "region1";
const QString RegionMapping::XMLE_REGION2 = "region2";
const QString RegionMapping::XMLE_ROOT = "MatchingResult";


class RegionMapping::RegionMappingPrivate
{

public:

    QList< QPair< int, int > > mapping;

    RegionMappingPrivate( RegionMapping *rm )
        : regionMapping( rm )
    {
    }

    virtual ~RegionMappingPrivate()
    {
    }

private:

    RegionMapping *regionMapping;

};

RegionMapping::RegionMapping(QObject *parent)
    : QObject(parent),
      d( new RegionMappingPrivate( this ) )
{
}

RegionMapping::~RegionMapping()
{
    delete d;
}


const QList< QPair< int, int > > &RegionMapping::mapping() const
{
    return d->mapping;
}

bool RegionMapping::readFromXML( const QString &xmlData )
{
    QDomDocument doc;
    if ( doc.setContent( xmlData ) )
    {
        QDomElement root = doc.documentElement();
        if ( root.nodeName() == XMLE_ROOT )
        {
            d->mapping.clear();
            QDomNodeList mappingsList = root.elementsByTagName( XMLE_MAPPING );
            if ( mappingsList.count() > 0 )
            {
                QDomElement mappings = mappingsList.at( 0 ).toElement();
                if ( !( mappings.isNull() ) )
                {
                    QDomNodeList pairs = mappings.elementsByTagName( XMLE_PAIR );
                    for ( int i = 0; i < pairs.count(); i++ )
                    {
                        QDomElement pair = pairs.at( i ).toElement();
                        if ( !( pair.isNull() ) )
                        {
                            QDomElement region1 = pair.firstChildElement( XMLE_REGION1 );
                            QDomElement region2 = pair.firstChildElement( XMLE_REGION2 );
                            if ( !( region1.isNull() || region2.isNull() ) )
                            {
                                QPair< int, int > map( region1.nodeValue().toInt(), region2.nodeValue().toInt() );
                                d->mapping.append( map );
                            }
                        }
                    }
                }
            }
            return true;
        }
    }
    return false;
}

bool RegionMapping::readFromXMLFile( const QString &fileName )
{
    QFile file( fileName );
    if ( file.exists() && file.isReadable() )
    {
        if ( file.open( QIODevice::ReadOnly ) )
        {
            QByteArray data = file.readAll();
            return readFromXML( QString( data ) );
        }
    }
    return false;
}
