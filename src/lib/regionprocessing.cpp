/*
    img2roi - Region of Interest extractor
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "regionprocessing.h"

#include "regionofinterest.h"

#include <QDebug>
#include <qmath.h>

#include <limits.h>

#define sqr(a) (a*a)

#define fsqr(a) ((double)((a)*(a)))
#define vecLength(p) ( sqrt( fsqr( (p).x() ) + fsqr( (p).y() ) ) )

class RegionProcessingHelper
{

public:

    static double minDistance( const QList< QPoint > &p1, const QList< QPoint > &p2 )
    {
        bool hasResult;
        double result = INFINITY;
        for ( int i = 0; i < p1.length(); i++ )
        {
            for ( int j = 0; j < p2.length(); j++ )
            {
                double dist = sqrt( sqr( (double) ( p1[ i ].x() - p2[ j ].x() ) ) +
                                    sqr( (double) ( p1[ i ].y() - p2[ j ].y() ) ) );
                if ( !( hasResult ) || ( result > dist ) )
                {
                    result = dist;
                    hasResult = true;
                }
            }
        }
        return result;
    }

    static double minDistance( const QRect &r1, const QRect &r2 )
    {
        if ( r1.intersects( r2 ) ||
             r1.contains( r2 ) ||
             r2.contains( r1 ) )
        {
            return 0.0;
        }
        bool hasResult = false;
        double result = INFINITY;
        for ( int i = 0; i < 4; i++ )
        {
            for ( int j = 0; j < 4; j++ )
            {
                QPoint p1;
                switch ( i )
                {
                case 0: p1 = r1.topLeft();
                case 1: p1 = r1.topRight();
                case 2: p1 = r1.bottomLeft();
                default: p1 = r1.bottomRight();
                }
                QPoint p2;
                switch ( j )
                {
                case 0: p2 = r2.topLeft();
                case 1: p2 = r2.topRight();
                case 2: p2 = r2.bottomLeft();
                default: p2 = r2.bottomRight();
                }
                double dist = sqrt( sqr( (double) ( p1.x() - p2.x() ) ) +
                                    sqr( (double) ( p1.y() - p2.y() ) ) );
                if ( !( hasResult ) || dist < result )
                {
                    result = dist;
                    hasResult = true;
                }
            }
        }
        return result;
    }

private:

    RegionProcessingHelper() {}
    RegionProcessingHelper( const RegionProcessingHelper & ) {}

};

QList< RegionOfInterest * > RegionProcessing::boundingBoxJoining( const QList<RegionOfInterest *> &regions, QObject *parent )
{
    QList< RegionOfInterest * > result;
    foreach ( RegionOfInterest *oldRegion, regions )
    {
        RegionOfInterest *region = new RegionOfInterest( parent );
        region->setPixels( oldRegion->pixels() );
        result.append( region );
    }

    while ( true )
    {
        bool repeat = false;
        for ( int i = 0; i < result.length() - 1; i++ )
        {
            for ( int j = i + 1; j < result.length(); j++ )
            {
                if ( result[ i ]->boundingBox().contains( result[ j ]->boundingBox() ) ||
                     result[ j ]->boundingBox().contains( result[ i ]->boundingBox() ) ||
                     result[ i ]->boundingBox().intersects( result[ j ]->boundingBox() ))
                {
                    result[ i ]->addPixels( result[ j ]->pixels() );
                    RegionOfInterest *region = result[ j ];
                    result.removeAll( region );
                    delete region;
                    repeat = true;
                }
            }
        }
        if ( !( repeat ) )
        {
            break;
        }
    }
    return result;
}

QList< RegionOfInterest * > RegionProcessing::boundingSphereJoining( const QList<RegionOfInterest *> &regions, QObject *parent )
{
    QList< RegionOfInterest * > result;
    foreach ( RegionOfInterest *oldRegion, regions )
    {
        RegionOfInterest *region = new RegionOfInterest( parent );
        region->setPixels( oldRegion->pixels() );
        result.append( region );
    }

    while ( true )
    {
        bool repeat = false;
        for ( int i = 0; i < result.length() - 1; i++ )
        {
            for ( int j = i + 1; j < result.length(); j++ )
            {
                if ( sqrt( sqr( ( double ) ( result[ i ]->boundingCircleCenter().x() - result[ j ]->boundingCircleCenter().x() ) ) +
                           sqr( ( double ) ( result[ i ]->boundingCircleCenter().y() - result[ j ]->boundingCircleCenter().y() ) ) )  -
                     result[ i ]->boundingCircleRadius() - result[ j ]->boundingCircleRadius() <= 0.0 )
                {
                    qDebug() << "Joining...";
                    result[ i ]->addPixels( result[ j ]->pixels() );
                    RegionOfInterest *region = result[ j ];
                    result.removeAll( region );
                    delete region;
                    repeat = true;
                }
            }
        }
        if ( !( repeat ) )
        {
            break;
        }
    }
    return result;
}

QList< RegionOfInterest * > RegionProcessing::gravityJoining( const QList<RegionOfInterest *> &regions, QObject *parent )
{
    QList< RegionOfInterest * > result;
    foreach ( RegionOfInterest *oldRegion, regions )
    {
        RegionOfInterest *region = new RegionOfInterest( parent );
        region->setPixels( oldRegion->pixels() );
        result.append( region );
    }

    while ( true )
    {
        bool repeat = false;

        for ( int i = 0; i < result.length() - 1; i++ )
        {
            for ( int j = i + 1; j < result.length(); j++ )
            {
                double r = vecLength( result[ i ]->centerOfGravity() - result[ j ]->centerOfGravity() );
                bool merge = false;
                if ( r == 0.0 )
                {
                    merge = true;
                } else
                {
                    double F = (qMin( 10, result[ i ]->pixels().count() ) * qMin( 10, result[ j ]->pixels().count() ) ) / sqr( r );
                    qDebug() << F;
                    if ( F >= 0.005 )
                    {
                        merge = true;
                    }
                }
                if ( merge )
                {
                    result[ i ]->addPixels( result[ j ]->pixels() );
                    RegionOfInterest *r = result[ j ];
                    result.removeAll( r );
                    delete r;
                    repeat = true;
                    qDebug() << "Merged!";
                }
            }
        }

        if ( !( repeat ) )
        {
            break;
        }
    }
    return result;
}
