/*
    img2roi - Region of Interest extractor
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REGIONOFINTEREST_H
#define REGIONOFINTEREST_H

#include "i2r_defines.h"

#include <QList>
#include <QObject>
#include <QPoint>
#include <QRect>

/**
  * @brief Encapsulates data about a region of interest.
  *
  * A single region of interest is e.g. a stroke in the image. It can basically
  * be described by the pixels belonging to the stroke.
  * However, the class provides additional information, as for example the
  * bounding box or center of gravity.
  */
class I2R_EXPORT RegionOfInterest : public QObject
{

    Q_OBJECT

public:

    /**
      * @brief Ctor.
      */
    RegionOfInterest( QObject *parent = 0 );

    /**
      * @brief Dtor.
      */
    virtual ~RegionOfInterest();

    /**
      * @brief Pixel data.
      *
      * Returns the pixels belonging to the region.
      */
    const QList< QPoint > &pixels() const;

    /**
      * @brief Center of gravity.
      *
      * Returns the center of gravity.
      */
    const QPoint &centerOfGravity() const;

    /**
      * @brief Returns the bounding box for the region.
      *
      * Returns the bounding box for the region. The box returned is axis-aligned
      * (an AABB).
      */
    const QRect &boundingBox() const;

    /**
      * @brief Center of bounding circle.
      *
      * Returns the center of the bounding circle, i.e. the smallest circle that
      * contains the complete region.
      */
    const QPoint &boundingCircleCenter() const;

    /**
      * @brief Radius of the bounding circle.
      *
      * Returns the radius of the bounding circle, i.e. the smallest circle that
      * contains the complete region.
      */
    const int &boundingCircleRadius() const;

    /**
      * @brief Returns the "north pole" of the region.
      *
      * A region has two special pixels: The ones which are farest away from each
      * other. The pixel which is nearer at the top of the input is assumed the north pole.
      *
      * @sa southPole()
      */
    const QPoint &northPole() const;

    /**
      * @brief Returns the "south pole" of the region.
      *
      * A region has two special pixels: The ones which are farest away from each
      * other. The pixel which is nearer at the bottom of the input is assumed the south pole.
      *
      * @sa northPole()
      */
    const QPoint &southPole() const;


public slots:

    /**
      * @brief Set pixel data.
      *
      * Replaces the pixel data currently hold by the region.
      */
    void setPixels( const QList< QPoint > &p );

    /**
      * @brief Add pixel.
      *
      * Adds a single pixel to the region.
      */
    void addPixel( const QPoint &p );

    /**
      * @brief Add pixels.
      *
      * Adds the list of pixels to the region.
      */
    void addPixels( const QList< QPoint > &p );

    /**
      * @brief Remove pixel.
      *
      * Removes a single pixel from the region.
      */
    void removePixel( const QPoint &p );

    /**
      * @brief Clear the region.
      *
      * Removes all pixels from the region.
      */
    void clearPixels();

private:

    class RegionOfInterestPrivate;
    RegionOfInterestPrivate *d;

};

#endif // REGIONOFINTEREST_H
