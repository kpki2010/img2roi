/*
    img2roi - Region of Interest extractor
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "regionexporter.h"

#include "regionofinterest.h"

#include <QDomDocument>
#include <QFile>
#include <QTextStream>

const QString RegionExporter::XML_DOCUMENT_TYPE         = "RegionsOfInterest";

const QString RegionExporter::XMLE_BOUNDING_BOX         = "boundingbox";
const QString RegionExporter::XMLE_BOUNDING_SPHERE      = "boundingsphere";
const QString RegionExporter::XMLE_CENTER_OF_GRAVITY    = "centerofgravity";
const QString RegionExporter::XMLE_NORTH_POLE           = "northpole";
const QString RegionExporter::XMLE_PIXEL                = "pixel";
const QString RegionExporter::XMLE_PIXELS               = "pixels";
const QString RegionExporter::XMLE_REGION               = "region";
const QString RegionExporter::XMLE_REGIONS              = "regions";
const QString RegionExporter::XMLE_ROOT_ELEMENT         = "regionsofinterest";
const QString RegionExporter::XMLE_SOUTH_POLE           = "southpole";

const QString RegionExporter::XMLA_HEIGHT               = "height";
const QString RegionExporter::XMLA_ID                   = "id";
const QString RegionExporter::XMLA_RADIUS               = "radius";
const QString RegionExporter::XMLA_WIDTH                = "width";
const QString RegionExporter::XMLA_X                    = "x";
const QString RegionExporter::XMLA_Y                    = "y";

RegionExporter::~RegionExporter()
{
}

QString RegionExporter::exportToXml( const QList<RegionOfInterest *> &regions )
{
    QDomDocument doc( XML_DOCUMENT_TYPE );

    QDomElement root = doc.createElement( XMLE_ROOT_ELEMENT );
    doc.appendChild( root );

    QDomElement regionsElement = doc.createElement( XMLE_REGIONS );
    root.appendChild( regionsElement );

    for ( int index = 0; index < regions.count(); index++ )
    {
        RegionOfInterest *region = regions[ index ];

        QDomElement regionElement = doc.createElement( XMLE_REGION );
        regionElement.setAttribute( XMLA_ID, index );
        regionsElement.appendChild( regionElement );

        QDomElement boundingBox = doc.createElement( XMLE_BOUNDING_BOX );
        boundingBox.setAttribute( XMLA_X, region->boundingBox().x() );
        boundingBox.setAttribute( XMLA_Y, region->boundingBox().y() );
        boundingBox.setAttribute( XMLA_WIDTH, region->boundingBox().width() );
        boundingBox.setAttribute( XMLA_HEIGHT, region->boundingBox().height() );
        regionElement.appendChild( boundingBox );

        QDomElement boundingSphere = doc.createElement( XMLE_BOUNDING_SPHERE );
        boundingSphere.setAttribute( XMLA_X, region->boundingCircleCenter().x() );
        boundingSphere.setAttribute( XMLA_Y, region->boundingCircleCenter().y() );
        boundingSphere.setAttribute( XMLA_RADIUS, region->boundingCircleRadius() );
        regionElement.appendChild( boundingSphere );

        QDomElement centerOfGravity = doc.createElement( XMLE_CENTER_OF_GRAVITY );
        centerOfGravity.setAttribute( XMLA_X, region->centerOfGravity().x() );
        centerOfGravity.setAttribute( XMLA_Y, region->centerOfGravity().y() );
        regionElement.appendChild( centerOfGravity );

        QDomElement northPole = doc.createElement( XMLE_NORTH_POLE );
        northPole.setAttribute( XMLA_X, region->northPole().x() );
        northPole.setAttribute( XMLA_Y, region->northPole().y() );
        regionElement.appendChild( northPole );

        QDomElement southPole = doc.createElement( XMLE_SOUTH_POLE );
        southPole.setAttribute( XMLA_X, region->southPole().x() );
        southPole.setAttribute( XMLA_Y, region->southPole().y() );
        regionElement.appendChild( southPole );

        QDomElement pixels = doc.createElement( XMLE_PIXELS );
        regionElement.appendChild( pixels );

        foreach ( QPoint p, region->pixels() )
        {
            QDomElement pixel = doc.createElement( XMLE_PIXEL );
            pixel.setAttribute( XMLA_X, p.x() );
            pixel.setAttribute( XMLA_Y, p.y() );
            pixels.appendChild( pixel );
        }
    }

    return doc.toString( 2 );
}

bool RegionExporter::exportToXml( const QList<RegionOfInterest *> &regions, const QString &filename )
{
    QFile file( filename );
    if ( file.open( QIODevice::WriteOnly ) )
    {
        QTextStream stream( &file );
        stream << exportToXml( regions );
        file.close();
        return true;
    }
    return false;
}

RegionExporter::RegionExporter()
{
}

RegionExporter::RegionExporter( const RegionExporter & )
{
}


