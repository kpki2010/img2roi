/*
    img2roi - Region of Interest extractor
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "regionextractor.h"

#include "regionofinterest.h"

#include <QColor>
#include <QDebug>
#include <QQueue>

class RegionExtractor::RegionExtractorPrivate
{

public:

    QList< RegionOfInterest * > regions;

    RegionExtractorPrivate( RegionExtractor *extractor )
        : regions(),
          regionExtractor( extractor )
    {

    }

    virtual ~RegionExtractorPrivate()
    {
    }

    void extract( const QImage &image )
    {
        qDebug() << "RegionExtractor: Clearing up buffers...";
        clearRegions();

        QImage working( image );

        qDebug() << "RegionExtractor: Extracting regions...";
        for ( int x = 0; x < working.width(); x++ )
        {
            for ( int y = 0; y < working.height(); y++ )
            {
                if ( working.pixel( x, y ) != QColor( Qt::white ).rgb() )
                {
                    extractSingleRegion( QPoint( x, y ), working );
                }
            }
        }
    }

private:

    RegionExtractor *regionExtractor;

    void extractSingleRegion( QPoint start, QImage &image )
    {
        RegionOfInterest *region = new RegionOfInterest( regionExtractor );
        QList< QPoint > pixbuf;
        QQueue< QPoint > queue;
        const QPoint neighbors[ 8 ] = { QPoint( -1, -1 ), QPoint( 0, -1 ), QPoint( 1, -1 ),
                                        QPoint( -1,  0 ),                  QPoint( 1,  0 ),
                                        QPoint( -1,  1 ), QPoint( 0,  1 ), QPoint( 1,  1 ) };

        queue.enqueue( start );

        qDebug() << "RegionExtractor: Ripping single region...";
        while ( !( queue.isEmpty() ) )
        {
            QPoint current = queue.dequeue();
            pixbuf.append( current );
            image.setPixel( current, QColor( Qt::white ).rgb() );

            for ( int i = 0; i < 8; i++ )
            {
                QPoint npxl = QPoint( current.x() + neighbors[ i ].x(), current.y() + neighbors[ i ].y() );
                if ( npxl.x() >= 0 && npxl.x() < image.width() && npxl.y() >= 0 && npxl.y() < image.height() )
                {
                    if ( image.pixel( npxl ) != QColor( Qt::white ).rgb() )
                    {
                        if ( !( queue.contains( npxl ) ) )
                        {
                            queue.enqueue( npxl );
                        }
                    }
                }
            }
        }
        region->setPixels( pixbuf );
        regions.append( region );
    }

    void clearRegions()
    {
        foreach ( RegionOfInterest *region, regions )
        {
            delete region;
        }
        regions.clear();
    }

};

RegionExtractor::RegionExtractor( QObject *parent )
    : QObject( parent ),
      d( new RegionExtractorPrivate( this ) )
{
}

RegionExtractor::~RegionExtractor()
{
    delete d;
}

const QList< RegionOfInterest * > &RegionExtractor::regions() const
{
    return d->regions;
}

void RegionExtractor::extract( const QImage &image )
{
    d->extract( image );
}
