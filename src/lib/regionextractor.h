/*
    img2roi - Region of Interest extractor
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REGIONEXTRACTOR_H
#define REGIONEXTRACTOR_H

#include "i2r_defines.h"

#include <QImage>
#include <QList>
#include <QObject>

class RegionOfInterest;

/**
  * @brief Simple region extractor.
  *
  * Extracts regions from an monochrome input image.
  * This simply extracts regions of coherent pixels, without further
  * processing (except the meta-data calculations done by the
  * RegionOfInterest class.
  */
class I2R_EXPORT RegionExtractor : public QObject
{

    Q_OBJECT

public:

    /**
      * @brief Ctor
      */
    RegionExtractor( QObject *parent = 0 );

    /**
      * @brief Dtor
      */
    virtual ~RegionExtractor();

    /**
      * @brief Extracted regions.
      *
      * The regions extracted while the last extract() call.
      */
    const QList< RegionOfInterest * > &regions() const;

    /**
      * @brief Extracts regions from an image.
      *
      * Extracts regions of coherent pixels from the given monochrome
      * image. The method assumes all black pixels as foreground. Every other
      * color is assumed background.
      *
      * The method will create a RegionOfInterest for each coherent bunch
      * of pixels found. Found regions can be accessed used the regions() method.
      */
    void extract( const QImage &image );

private:

    class RegionExtractorPrivate;
    RegionExtractorPrivate *d;

};

#endif // REGIONEXTRACTOR_H
