/*
    img2roi - Region of Interest extractor
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REGIONIMPORTER_H
#define REGIONIMPORTER_H

#include "i2r_defines.h"

#include <QList>
#include <QObject>

class I2R_EXPORT RegionOfInterest;

/**
  * @brief Import regions of interest.
  *
  * This class provides methods to import regions of interest exported using the
  * RegionExporter.
  */
class RegionImporter
{

public:

    virtual ~RegionImporter();

    static QList< RegionOfInterest * > importFromXml( const QString &xml, QObject *parent = 0 );
    static QList< RegionOfInterest * > importFromXmlFile( const QString &filename, QObject *parent = 0 );

private:

    RegionImporter();
    RegionImporter( const RegionImporter & );

};

#endif // REGIONIMPORTER_H
