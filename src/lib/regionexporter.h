/*
    img2roi - Region of Interest extractor
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REGIONEXPORTER_H
#define REGIONEXPORTER_H

#include "i2r_defines.h"

#include <QList>

class RegionOfInterest;

/**
  * @brief Export regions of interest.
  *
  * This abstract class provides methods to export extracted regions of interest
  * to formats, that easily can be read by other programs.
  */
class I2R_EXPORT RegionExporter
{

public:

    static const QString XML_DOCUMENT_TYPE;

    static const QString XMLE_BOUNDING_BOX;
    static const QString XMLE_BOUNDING_SPHERE;
    static const QString XMLE_CENTER_OF_GRAVITY;
    static const QString XMLE_NORTH_POLE;
    static const QString XMLE_PIXEL;
    static const QString XMLE_PIXELS;
    static const QString XMLE_REGION;
    static const QString XMLE_REGIONS;
    static const QString XMLE_ROOT_ELEMENT;
    static const QString XMLE_SOUTH_POLE;

    static const QString XMLA_HEIGHT;
    static const QString XMLA_ID;
    static const QString XMLA_RADIUS;
    static const QString XMLA_WIDTH;
    static const QString XMLA_X;
    static const QString XMLA_Y;

    /**
      * @brief Dtor.
      */
    virtual ~RegionExporter();

    /**
      * @brief Export regions of interest to XML.
      *
      * Create a XML representation of the list of regions as a string.
      *
      * @param regions The regions to export.
      *
      * @returns A XML representation of the regions.
      */
    static QString exportToXml( const QList< RegionOfInterest * > &regions );

    /**
      * @brief Export regions of interest to XML file.
      *
      * Writes the list of regions to the given file, using XML formatting.
      *
      * @param regions The regions to export.
      * @param filename The file to write to.
      *
      * @returns True of writing was successfull, otherwise false.
      **/
    static bool exportToXml( const QList< RegionOfInterest * > &regions, const QString &filename );

private:

    RegionExporter();
    RegionExporter( const RegionExporter & );
};

#endif // REGIONEXPORTER_H
