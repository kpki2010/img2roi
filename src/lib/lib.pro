TEMPLATE = lib
CONFIG += plugin
QT += core \
    xml \
    gui
CONFIG(debug, debug|release):DESTDIR = ../../bin/Debug
else:DESTDIR = ../../bin/Release
TARGET = img2roiutils
HEADERS += i2r_defines.h \
    regionofinterest.h \
    regionextractor.h \
    regionrenderer.h \
    regionexporter.h \
    regionimporter.h \
    regionprocessing.h \
    regionmapping.h
DEFINES += IMG2ROIUTILS
SOURCES += regionofinterest.cpp \
    regionextractor.cpp \
    regionrenderer.cpp \
    regionexporter.cpp \
    regionimporter.cpp \
    regionprocessing.cpp \
    regionmapping.cpp
