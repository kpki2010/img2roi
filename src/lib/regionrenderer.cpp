/*
    img2roi - Region of Interest extractor
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "regionrenderer.h"

#include "regionofinterest.h"

#include <QBrush>
#include <QColor>
#include <QDebug>
#include <QPainter>
#include <QPen>

RegionRenderer::~RegionRenderer()
{

}

QImage RegionRenderer::render( const QImage &original, const QList<RegionOfInterest *> &regions )
{
    qDebug() << "RegionRenderer: Creating working copy of input image...";
    QImage result( original );
    if ( !( result.isNull() ) )
    {
        qDebug() << "RegionRenderer: Initializing painter...";
        QPainter painter( &result );
        QBrush brush( Qt::NoBrush );
        painter.setBrush( brush );
        QPen pen( Qt::SolidLine );
        painter.setPen( pen );

        qDebug() << "RegionRenderer: Rendering regions...";
        foreach ( RegionOfInterest *region, regions )
        {
            int transparency = 100;

            painter.setPen( QColor( 0xff, 0x00, 0x00, transparency ) );
            painter.drawRect( region->boundingBox() );

            painter.setPen( QColor( 0x00, 0x00, 0xff, transparency ) );
            painter.drawEllipse( region->centerOfGravity(), 10, 10 );

            painter.setPen( QColor( 0x00, 0xff, 0x00, transparency ) );
            painter.drawEllipse( region->boundingCircleCenter(), region->boundingCircleRadius(), region->boundingCircleRadius() );

            painter.setPen( QColor( 0xff, 0xbb, 0x00, transparency ) );
            painter.drawLine( region->northPole(), region->southPole() );
        }
    }
    qDebug() << "RegionRenderer: Done!";
    return result;
}

RegionRenderer::RegionRenderer()
{
}

RegionRenderer::RegionRenderer(const RegionRenderer &)
{

}
