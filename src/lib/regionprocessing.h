/*
    img2roi - Region of Interest extractor
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REGIONPROCESSING_H
#define REGIONPROCESSING_H

#include "i2r_defines.h"

#include <QList>

class RegionOfInterest;
class QObject;

/**
  * @brief Processing of a set of regions of interest.
  *
  * This abstract class provides methods to process and alter
  * a set of regions of interest.
  */
class I2R_EXPORT RegionProcessing
{

public:

    /**
      * @brief Join regions using a simple bounding-box test.
      *
      * This test uses the bounding boxes of the regions to test whether
      * two regions can be joined.
      *
      * It will join two regions if their boinding boxed overlap each other.
      *
      * @param regions The regions to process.
      * @param parent The parent object for the resulting regions.
      *
      * @returns A new list of regions.
      *
      * @note This method will create a completely new list of regions instead of altering
      *       existing ones!
      */
    static QList< RegionOfInterest * > boundingBoxJoining( const QList< RegionOfInterest * > &regions, QObject *parent = 0 );

    /**
      * @brief Join regions using a simple boinding-sphere test.
      *
      * This test uses a simple bounding sphere intersection test to determine regions to
      * merge.
      *
      * @param regions The regions to process.
      * @param parent The parent object for the resulting regions.
      *
      * @returns A new list of regions.
      *
      * @note This method will create a completely new list of regions instead of altering
      * existing ones!
      */
    static QList< RegionOfInterest * > boundingSphereJoining( const QList< RegionOfInterest * > &regions, QObject *parent = 0 );

    /**
      * @brief Join regions using a gravity approach.
      *
      * This will try to merge regions based on their weight and their distance. The heavier two regions are and the
      * nearer they are at each other, it is more likely they belong to each other and could be joined.
      *
      * @param regions The list of regions to test for joining.
      * @param parent An parent object for the resulting list of regions.
      *
      * @returns A list with (possibly) joined regions creatied from the input list.
      *
      * @note This method will create a completely new list of regions instead of altering
      *       existing ones!
      */
    static QList< RegionOfInterest * > gravityJoining( const QList< RegionOfInterest * > &regions, QObject *parent = 0 );

private:

    RegionProcessing() {}
    RegionProcessing( const RegionProcessing & ) {}
};

#endif // REGIONPROCESSING_H
