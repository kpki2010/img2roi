/*
    img2roi - Region of Interest extractor
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "regionimporter.h"

#include "regionexporter.h"
#include "regionofinterest.h"

#include <QDomDocument>
#include <QFile>

RegionImporter::~RegionImporter()
{
}

QList< RegionOfInterest * > RegionImporter::importFromXml( const QString &xml, QObject *parent )
{
    QList< RegionOfInterest * > result;

    QDomDocument doc( RegionExporter::XML_DOCUMENT_TYPE );
    if ( doc.setContent( xml ) )
    {
        QDomElement root = doc.documentElement();
        if ( root.tagName() == RegionExporter::XMLE_ROOT_ELEMENT )
        {
            QDomElement regions = root.elementsByTagName( RegionExporter::XMLE_REGIONS ).item( 0 ).toElement();
            if ( !( regions.isNull() ) )
            {
                QDomNodeList regionList = regions.elementsByTagName( RegionExporter::XMLE_REGION );
                for ( int i = 0; i < regionList.size(); i++ )
                {
                    QDomElement region = regionList.item( i ).toElement();
                    if ( !( region.isNull() ) )
                    {
                        RegionOfInterest *r = new RegionOfInterest( parent );
                        QList< QPoint > pxls;
                        QDomNodeList pixels = region.elementsByTagName( RegionExporter::XMLE_PIXEL );
                        for ( int j = 0; j < pixels.size(); j++ )
                        {
                            QDomElement pixel = pixels.item( j ).toElement();
                            if ( !( pixel.isNull() ) )
                            {
                                QPoint p;
                                p.setX( pixel.attribute( RegionExporter::XMLA_X, "0" ).toInt() );
                                p.setY( pixel.attribute( RegionExporter::XMLA_Y, "0" ).toInt() );
                                pxls.append( p );
                            }
                        }
                        r->setPixels( pxls );
                        result.append( r );
                    }
                }
            }
        }
    }
    return result;
}

QList< RegionOfInterest * > RegionImporter::importFromXmlFile( const QString &filename, QObject *parent )
{
    QFile file( filename );
    QString xml;
    if ( file.open( QIODevice::ReadOnly ) )
    {
        xml = QString( file.readAll() );
    }
    file.close();
    return RegionImporter::importFromXml( xml, parent );
}

RegionImporter::RegionImporter()
{
}

RegionImporter::RegionImporter(const RegionImporter &)
{

}
